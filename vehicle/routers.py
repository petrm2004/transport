from rest_framework import routers
from .views import VehicleModelViewSet, ImportRegistryViewSet

router = routers.DefaultRouter()
router.register(r'vehicle_model', VehicleModelViewSet)
router.register(r'import_registry', ImportRegistryViewSet)

