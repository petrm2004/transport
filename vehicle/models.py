from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator, RegexValidator
from django.db import models


class IntegerFieldCustom(models.IntegerField):
    def __init__(self, *args, **kwargs):
        self.name_xlsx = kwargs.get('name_xlsx', '')
        kwargs = {key: value for key, value in kwargs.items() if key != 'name_xlsx'}
        self.name_csv = kwargs.get('name_csv', '')
        kwargs = {key: value for key, value in kwargs.items() if key != 'name_csv'}
        super(IntegerFieldCustom, self).__init__(*args, **kwargs)


class CharFieldCustom(models.CharField):
    def __init__(self, *args, **kwargs):
        self.name_xlsx = kwargs.get('name_xlsx', '')
        kwargs = {key: value for key, value in kwargs.items() if key != 'name_xlsx'}
        self.name_csv = kwargs.get('name_csv', '')
        kwargs = {key: value for key, value in kwargs.items() if key != 'name_csv'}
        super(CharFieldCustom, self).__init__(*args, **kwargs)


class DateFieldCustom(models.DateField):
    def __init__(self, *args, **kwargs):
        self.name_xlsx = kwargs.get('name_xlsx', '')
        kwargs = {key: value for key, value in kwargs.items() if key != 'name_xlsx'}
        self.name_csv = kwargs.get('name_csv', '')
        kwargs = {key: value for key, value in kwargs.items() if key != 'name_csv'}
        super(DateFieldCustom, self).__init__(*args, **kwargs)


class Mark(models.Model):
    name = models.CharField(max_length=255, verbose_name='Наименование марки')

    def __str__(self):
        return self.name


class ModelCar(models.Model):
    name = models.CharField(max_length=255, verbose_name='Наименование модели')

    def __str__(self):
        return self.name


class Color(models.Model):
    name = models.CharField(max_length=255, verbose_name='Цвет')

    def __str__(self):
        return self.name


ctc_number_validator = RegexValidator(regex=r'^[\d]{6}$',
                                 message="XXXXXX (X - цифра от 0 до 9)")

registration_number_validator = RegexValidator(
    regex=r'^[АВЕКМНОРСТУХ]{1}[\d]{3}[АВЕКМНОРСТУХ]{2}[\d]{2,3}$',
    message="SXXXSSXX или SXXXSSXXX (X - цифра от 0 до 9, S - символы на русской раскладке: А, В, Е, К, М, Н, О, Р, С, Т, У и Х)")

vin_validator = RegexValidator(
    regex=r'^[\dABCDEFGHJKLMNPRSTUVWXYZ]{17}$',
    message="Значение должно быть длинной в 17 сиволов. Допустимые сиволы: цифры и A, B, C, D, E, F, G, H, J, K, L, M, N, P, R, S, T, U, V, W, X, Y, Z")


class AbstractVehicleModel(models.Model):
    mark = models.ForeignKey(Mark, on_delete=models.PROTECT, verbose_name='Марка')
    model_car = models.ForeignKey(ModelCar, on_delete=models.PROTECT, verbose_name='Модель')
    color = models.ForeignKey(Color, on_delete=models.PROTECT, verbose_name='Цвет')
    registration_number = models.CharField(max_length=9, verbose_name='Регистрационный номер',
                                           validators=[registration_number_validator])
    issue_year = models.IntegerField(verbose_name='Год выпуска', validators=[MinValueValidator(1900)])
    vin = models.CharField(max_length=17, verbose_name='VIN',validators=[vin_validator])
    ctc_number = models.CharField(max_length=6, verbose_name='Номер СТС', validators=[ctc_number_validator])
    ctc_date = models.DateField(verbose_name='Дата СТС')

    class Meta:
        abstract = True


class VehicleModel(AbstractVehicleModel):

    class Meta:
        verbose_name = 'Модель ТС'


class ImportRegistry(models.Model):
    DOWNLOADED = 'downloaded'
    PROCESSING1 = 'processing1'
    PROCESSING2 = 'processing2'
    PROCESSING3 = 'processing3'
    NOT_ACCEPTED = 'not_accepted'
    ACCEPTED = 'accepted'
    PROCESS_STATUS = [
        (DOWNLOADED, 'Загружен'),
        (PROCESSING1, 'Обрабатывается (шаг 1)'),
        (PROCESSING2, 'Обрабатывается (шаг 2)'),
        (PROCESSING3, 'Обрабатывается (шаг 3)'),
        (NOT_ACCEPTED, 'Не принят'),
        (ACCEPTED, 'Реестр принят')
    ]
    CSV = 'csv'
    XLSX = 'xlsx'
    FILE_TYPE = [(CSV, 'csv'), (XLSX, 'xlsx')]

    create_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    file_path = models.FileField(upload_to='vehicle/registries/%Y/%m/%d/',
                                 max_length=255, verbose_name='Файл импорта')
    filename = models.CharField(max_length=100, null=True, verbose_name='Имя файл импорта')
    process_status = models.CharField(max_length=20, choices=PROCESS_STATUS, default=DOWNLOADED, verbose_name='Статус обработки')
    file_type = models.CharField(max_length=4, choices=FILE_TYPE, verbose_name='Тип реестра')
    protocol_path = models.FileField(upload_to='vehicle/protocols/%Y/%m/%d/', max_length=255, blank=True,
                                     null=True,
                                     verbose_name='Файл протокола')

    class Meta:
        verbose_name = 'Импорт реестра ТС'


class VehicleModelImport(AbstractVehicleModel):
    mark = models.ForeignKey(Mark, on_delete=models.PROTECT, verbose_name='Марка', null=True, blank=True)
    model_car = models.ForeignKey(ModelCar, on_delete=models.PROTECT, verbose_name='Модель', null=True, blank=True)
    color = models.ForeignKey(Color, on_delete=models.PROTECT, verbose_name='Цвет', null=True, blank=True)

    mark_name = CharFieldCustom(max_length=255, verbose_name='Наименование марки',
                                name_xlsx='Марка',
                                name_csv='mark')
    model_car_name = CharFieldCustom(max_length=255, verbose_name='Наименование модели',
                                     name_xlsx='Модель',
                                     name_csv='model_car')
    color_name = CharFieldCustom(max_length=255, verbose_name='Наименование цвета',
                                 name_xlsx='Наименование цвета',
                                 name_csv='color')
    registration_number = CharFieldCustom(max_length=9, verbose_name='Регистрационный номер',
                                          validators=[registration_number_validator],
                                          name_xlsx='Регистрационный номер',
                                          name_csv='registration_number')
    issue_year = IntegerFieldCustom(verbose_name='Год выпуска', validators=[MinValueValidator(1900)],
                                    name_xlsx='Год выпуска',
                                    name_csv='issue_year')
    vin = CharFieldCustom(max_length=17, verbose_name='VIN', validators=[vin_validator],
                          name_xlsx='VIN',
                          name_csv='vin')
    ctc_number = CharFieldCustom(max_length=6, verbose_name='Номер СТС', validators=[ctc_number_validator],
                                 name_xlsx='Номер СТС',
                                 name_csv='ctc_number')
    ctc_date = DateFieldCustom(verbose_name='Дата СТС',
                               name_xlsx='Дата СТС',
                               name_csv='ctc_date')

    row_num = models.IntegerField(verbose_name='Номер строки в реестре')
    errors = models.TextField(default='', blank=True, verbose_name='Ошибки импорта')

    class Meta:
        verbose_name = 'Модель ТС для импорта'

    def get_fields(self, type):
        """
        :param self: Используем сам класс Utilities
        :param type: name_xlsx или name_csv
        :return: Возвращается список полей 'name_xls' типа set
        """
        fields = []
        for field in self._meta.fields:
            name = getattr(field, type, None)
            if name:
                fields.append(getattr(field, 'attname', None))
        return fields

    def get_column(self, field_name, type):
        """
        :param field_name:
        :param type: name_xlsx или name_csv
        :return: Возвращает наименование столбца в соответствии с заданым type
        """
        for field in self._meta.fields:
            name = getattr(field, type, None)
            if name:
                attname = getattr(field, 'attname', None)
                if attname == field_name:
                    return name

    def clean_variable(self):
        for field in self._meta.fields:
            name = getattr(field, 'name_xlsx', None)
            attname = field.attname
            field_value = getattr(self, attname)
            if name:
                if (field_value == '') and getattr(field, 'blank', False) and isinstance(field, IntegerFieldCustom):
                    setattr(self, attname, None)
                if (field_value == None) and isinstance(field, CharFieldCustom):
                    setattr(self, attname, '')
                # Решение проблемы входных данных float в текстовые поля, иначе значение приезжает так 160061.0
                if (field_value is not None) and isinstance(field_value, float) and isinstance(field, CharFieldCustom):
                    value = str(round(field_value))
                    setattr(self, attname, value)

    def check_type_variable(self):
        errors = {}
        PROTCCOL_INTEGER_NUMBER = 'Убедитесь, что данное значение является целым числом'
        for field in self._meta.fields:
            name = getattr(field, 'name_xlsx', None)
            attname = field.attname
            field_value = getattr(self, attname)
            if field_value is not None and isinstance(field_value, float) and \
                    name and isinstance(field, IntegerFieldCustom) and field_value != round(field_value):
                errors[attname] = ValidationError(PROTCCOL_INTEGER_NUMBER)
        if errors:
            raise ValidationError(errors)



