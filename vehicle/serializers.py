from rest_framework import serializers

from vehicle.models import VehicleModel, ImportRegistry


class VehicleModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = VehicleModel
        fields = ('id', 'mark', 'model_car', 'color', 'registration_number', 'issue_year', 'vin',
                  'ctc_number', 'ctc_date')
        read_only_fields = ('id',)


class ImportRegistrySerializer(serializers.ModelSerializer):

    class Meta:
        model = ImportRegistry
        fields = ('id', 'create_date', 'filename', 'process_status', 'protocol_path',
                  'file_path', 'file_type', )
        read_only_fields = ('id', 'create_date', 'filename', 'process_status', 'protocol_path',)



