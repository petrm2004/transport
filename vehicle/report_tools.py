from openpyxl.cell import WriteOnlyCell
from openpyxl.styles import NamedStyle, Font, Side, Border, Alignment


def cell_style(sh, value, style, number_format=None):
    cell = WriteOnlyCell(sh, value=value)
    cell.style = style
    if number_format is not None:
        cell.number_format = number_format
    return cell


def int2(value):
    """
    Пытаемся перевести в int, иначе оставляем как есть
    :param value:
    :return:
    """
    try:
        result = int(value)
    except:
        result = value
    return result


class ExcelStyle(object):

    @staticmethod
    def table_style0():
        # Стиль
        table_style = NamedStyle(name="table_style0")
        table_style.font = Font(name='Arial Cyr', size=10)
        bd = Side(style='thin')
        table_style.border = Border(left=bd, top=bd, right=bd, bottom=bd)
        table_style.alignment = Alignment(wrap_text=True)
        return table_style