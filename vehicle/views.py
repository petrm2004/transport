import csv
import os

from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from openpyxl import load_workbook
from openpyxl.writer.excel import save_virtual_workbook
from rest_framework import viewsets, permissions, filters
from rest_framework.decorators import action
from rest_framework.response import Response

from vehicle.models import VehicleModel, ImportRegistry
from vehicle.report_tools import ExcelStyle, cell_style
from vehicle.serializers import VehicleModelSerializer, ImportRegistrySerializer
from vehicle.tools import import_file


class VehicleModelViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = VehicleModel.objects.all()
    serializer_class = VehicleModelSerializer
    permission_classes = [permissions.IsAuthenticated]
    filter_backends = (filters.SearchFilter,)
    search_fields = ('mark__name', 'registration_number', 'ctc_number',)

    @action(detail=False, methods=['get'], permission_classes=[permissions.IsAuthenticated])
    def get_report_xlsx(self, request):
        queryset = self.get_queryset().all()
        template_filename = os.path.join(settings.BASE_DIR, 'vehicle', 'templates', 'TC.xlsx')
        wb = load_workbook(template_filename)
        sh = wb.worksheets[0]
        table_style0 = ExcelStyle.table_style0()
        idx0 = 2
        for idx, i in enumerate(queryset, idx0):
            sh.append([cell_style(sh, i.mark.name, table_style0),
                       cell_style(sh, i.model_car.name, table_style0),
                       cell_style(sh, i.color.name, table_style0),
                       cell_style(sh, i.registration_number, table_style0),
                       cell_style(sh, i.issue_year, table_style0),
                       cell_style(sh, i.vin, table_style0),
                       cell_style(sh, i.ctc_number, table_style0),
                       cell_style(sh, i.ctc_date, table_style0, number_format='DD.MM.YYYY'),
                       ])
        response = HttpResponse(content=save_virtual_workbook(wb),
                                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
        response['Content-Disposition'] = 'attachment; filename=TC.xlsx'
        return response

    @action(detail=False, methods=['get'], permission_classes=[permissions.IsAuthenticated])
    def get_report_csv(self, request):
        queryset = self.get_queryset().all()
        response = HttpResponse(
            content_type='text/csv',
            headers={'Content-Disposition': 'attachment; filename="TC.csv"'},
        )
        writer = csv.writer(response)
        for i in queryset:
            writer.writerow([
                i.mark.name,
                i.model_car.name,
                i.color.name,
                i.registration_number,
                i.issue_year,
                i.vin,
                i.ctc_number,
                i.ctc_date,
            ])
        return response


class ImportRegistryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = ImportRegistry.objects.all()
    serializer_class = ImportRegistrySerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=['get'], permission_classes=[permissions.IsAuthenticated])
    def processing(self, request, pk=None):
        import_file(pk)
        return Response()





