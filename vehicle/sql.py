sql_check1 = """
with double0 as (
    select registration_number
    from vehicle_vehiclemodelimport
    group by registration_number
    having count(id) > 1
    )
update vehicle_vehiclemodelimport
SET errors = errors || ' Имеются строки с одинаковым регистрационным номером.'
where exists(
    select 1 from double0
    where
      double0.registration_number = vehicle_vehiclemodelimport.registration_number
        )
"""

sql_check2 = """
update vehicle_vehiclemodelimport
SET errors = errors || ' По данному регистрационному номеру сведения имеются в базе данных.'
where exists(
    select 1
    from vehicle_vehiclemodel
    where vehicle_vehiclemodel.registration_number = vehicle_vehiclemodelimport.registration_number
          )
"""

sql_get_errors = """
select row_num, errors
from vehicle_vehiclemodelimport
where errors <> ''
"""

sql_insert_mark = """
insert into vehicle_mark (name)
select
  tmp.mark_name
from
  vehicle_vehiclemodelimport tmp
  left join vehicle_mark mark on (tmp.mark_name = mark.name)
where
    mark.id is null
group by
    tmp.mark_name
"""

sql_insert_model_car = """
insert into vehicle_modelcar (name)
select
  tmp.model_car_name
from
  vehicle_vehiclemodelimport tmp
  left join vehicle_modelcar modelcar on (tmp.model_car_name = modelcar.name)
where
    modelcar.id is null
group by
    tmp.model_car_name
"""

sql_insert_color = """
insert into vehicle_color (name)
select
  tmp.color_name
from
  vehicle_vehiclemodelimport tmp
  left join vehicle_color color on (tmp.color_name = color.name)
where
    color.id is null
group by
    tmp.color_name
"""

sql_update_mark = """
update vehicle_vehiclemodelimport
SET mark_id =  (select vehicle_mark.id
                FROM vehicle_mark
                where vehicle_vehiclemodelimport.mark_name = vehicle_mark.name)
where
    EXISTS (
        SELECT 1
        FROM vehicle_mark
        WHERE vehicle_vehiclemodelimport.mark_name = vehicle_mark.name
    )
"""

sql_update_model_car = """
update vehicle_vehiclemodelimport
SET model_car_id = (select vehicle_modelcar.id
                    FROM vehicle_modelcar
                    where vehicle_vehiclemodelimport.model_car_name = vehicle_modelcar.name)
where exists(
    select 1
                    FROM vehicle_modelcar
                    where vehicle_vehiclemodelimport.model_car_name = vehicle_modelcar.name
          )
"""

sql_update_color = """
update vehicle_vehiclemodelimport
SET color_id = (select vehicle_color.id
                FROM vehicle_color
                where vehicle_vehiclemodelimport.color_name = vehicle_color.name)
where exists(
    select 1
                FROM vehicle_color
                where vehicle_vehiclemodelimport.color_name = vehicle_color.name
          )
"""

sql_insert_vehiclemodel = """
insert into vehicle_vehiclemodel (registration_number, issue_year, vin, ctc_number, ctc_date, color_id, mark_id, model_car_id)
select
  registration_number, issue_year, vin, ctc_number, ctc_date, color_id, mark_id, model_car_id
from
  vehicle_vehiclemodelimport tmp
"""
