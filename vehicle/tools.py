import csv
import pathlib
from io import StringIO

from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db import connections, connection
from openpyxl import load_workbook
from openpyxl.utils.exceptions import InvalidFileException

from vehicle.models import VehicleModelImport, ImportRegistry
from vehicle.sql import sql_check1, sql_get_errors, sql_check2, sql_insert_mark, sql_insert_model_car, sql_insert_color, \
    sql_update_mark, sql_update_model_car, sql_update_color, sql_insert_vehiclemodel


def import_file(id_registry):
    import_registry_temptable(id_registry)
    validate_registry(id_registry)
    import_registry_table(id_registry)


def save_registry(registry, process_status, file_type=None, protocol=None):
    registry.process_status = process_status
    if file_type:
        registry.file_type = file_type
    if protocol:
        file_io = StringIO(protocol)
        length_file_io = len(file_io.getvalue())
        registry.protocol_path = InMemoryUploadedFile(
            file_io,
            None,
            'protocol error.txt',
            'text/plain',
            length_file_io,
            None)
    registry.save()


def import_registry_temptable(id_registry):
    """
    Импорнт данных во временную таблицу. Приведение нужных типов. Валидация данных на уровне строки.
    :param id_registry:
    :return:
    """
    try:
        registry = ImportRegistry.objects.get(pk=id_registry, process_status=ImportRegistry.DOWNLOADED)
    except ObjectDoesNotExist as e:
        return
    save_registry(registry, ImportRegistry.PROCESSING1)
    model = VehicleModelImport
    model.objects.filter().delete()
    errors = []
    filepath = registry.file_path.path
    filename_suffix = pathlib.Path(filepath).suffix.lower()
    # Чтение данных файла и загрузка данных во временную таблицу
    if registry.file_type == ImportRegistry.XLSX:
        if filename_suffix != '.xlsx':
            save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol='Расширение файла должно быть "xlsx".')
            return
        try:
            wb = load_workbook(filepath)
            for row in wb.worksheets[0].rows:
                if row[0].row == 1:
                    continue
                item = model()
                name_fields_xls = model.get_fields(model, 'name_xlsx')
                for index, field0 in enumerate(name_fields_xls):
                    value = row[index].value
                    setattr(item, field0, value)
                try:
                    item.row_num = row[0].row
                    item.clean_variable()
                    item.check_type_variable()
                    item.full_clean()
                    item.save()
                except ValidationError as e:
                    for key, value in e.message_dict.items():
                        name_field = model.get_column(model, key, 'name_xlsx')
                        message = 'В строке {0} в поле "{1}" ошибка: {2}'.format(
                            row[0].row,
                            name_field,
                            ' '.join(value)
                        )
                        errors.append(message)
                except Exception as e:
                    errors.append(str(e))
            if errors:
                messages = '\r\n'.join(errors)
                save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol=messages)
                return
        except InvalidFileException as e:
            save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol='Некорректный тип файла.')
            return
        except Exception as e:
            save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol=str(e))
            return
        save_registry(registry, ImportRegistry.PROCESSING2)
    elif registry.file_type == ImportRegistry.CSV:
        if filename_suffix != '.csv':
            save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol='Расширение файла должно быть "csv".')
            return
        try:
            with open(filepath, 'r', newline='') as csvfile:
                data_csv = csv.reader(csvfile)
                for index_row, row in enumerate(data_csv, 1):
                    item = model()
                    name_fields_csv = model.get_fields(model, 'name_csv')
                    for index_field, field0 in enumerate(name_fields_csv):
                        value = row[index_field]
                        setattr(item, field0, value)
                    try:
                        item.row_num = index_row
                        item.clean_variable()
                        item.check_type_variable()
                        item.full_clean()
                        item.save()
                    except ValidationError as e:
                        for key, value in e.message_dict.items():
                            name_field = model.get_column(model, key, 'name_xlsx')
                            message = 'В строке {0} в поле "{1}" ошибка: {2}'.format(
                                index_row,
                                name_field,
                                ' '.join(value)
                            )
                            errors.append(message)
                    except Exception as e:
                        errors.append(str(e))
                if errors:
                    messages = '\r\n'.join(errors)
                    save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol=messages)
                    return
        except InvalidFileException as e:
            save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol='Некорректный тип файла.')
            return
        except Exception as e:
            save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol=str(e))
            return
        save_registry(registry, ImportRegistry.PROCESSING2)


def validate_registry(id_registry):
    """
    Валидация временной таблицы:
     на дублируюшие строчки,
     на пересечение с основной таблицей.
    :param id_registry:
    :return:
    """
    try:
        registry = ImportRegistry.objects.get(pk=id_registry, process_status=ImportRegistry.PROCESSING2)
    except ObjectDoesNotExist as e:
        return

    with connection.cursor() as cursor:
        cursor.execute(sql_check1)
        cursor.executescript(sql_check2)
        cursor.execute(sql_get_errors)
        errors_table = cursor.fetchall()
        errors = []
        for err in errors_table:
            errors.append('В строке {0} ошибка: {1}'.format(*err))
    if errors:
        messages = '\r\n'.join(errors)
        save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol=messages)
    else:
        save_registry(registry, ImportRegistry.PROCESSING3)


def import_registry_table(id_registry):
    """
    Заполнение справочников.
    Перенос данных из временной таблицы в основную.
    :param id_registry:
    :return:
    """
    try:
        registry = ImportRegistry.objects.get(pk=id_registry, process_status=ImportRegistry.PROCESSING3)
    except ObjectDoesNotExist as e:
        return
    try:
        with connections['default'].cursor() as cursor:
            cursor.execute(sql_insert_mark)
            cursor.execute(sql_insert_model_car)
            cursor.execute(sql_insert_color)
            cursor.execute(sql_update_mark)
            cursor.execute(sql_update_model_car)
            cursor.execute(sql_update_color)
            cursor.execute(sql_insert_vehiclemodel)

    except Exception as e:
        messages = str(e)
        save_registry(registry, ImportRegistry.NOT_ACCEPTED, protocol=messages)
        VehicleModelImport.objects.all().delete()
        return
    save_registry(registry, ImportRegistry.ACCEPTED)
    VehicleModelImport.objects.all().delete()


